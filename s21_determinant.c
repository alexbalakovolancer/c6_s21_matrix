#include "s21_matrix.h"

int s21_determinant(matrix_t *A, double *result) {
  if (isNotCorrect(*A)) return INVALID_MATRIX;
  if (A->columns != A->rows) return COMPUTATION_ERROR;

  *result = recursiveDeterminant(*A);

  return OK;
}