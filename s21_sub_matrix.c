#include "s21_matrix.h"

int s21_sub_matrix(matrix_t *A, matrix_t *B, matrix_t *result) {
  if (isNotCorrect(*A) || isNotCorrect(*B)) return INVALID_MATRIX;

  result->matrix = NULL;
  result->rows = 0;
  result->columns = 0;

  if (A->rows != B->rows || A->columns != B->columns) return COMPUTATION_ERROR;

  s21_create_matrix(A->rows, A->columns, result);
  for (int i = 0; i < A->rows; i++)
    for (int j = 0; j < A->columns; j++)
      result->matrix[i][j] = A->matrix[i][j] - B->matrix[i][j];

  return OK;
}