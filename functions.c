#include "s21_matrix.h"

matrix_t matrixCutCopy(matrix_t A, int row, int column) {
  matrix_t result;
  s21_create_matrix(A.rows - 1, A.columns - 1, &result);

  int i_new = 0;
  int j_new = 0;

  for (int i = 0; i < A.rows; i++) {
    if (i == row) continue;
    j_new = 0;
    for (int j = 0; j < A.columns; j++) {
      if (j == column) continue;
      result.matrix[i_new][j_new++] = A.matrix[i][j];
    }
    i_new++;
  }

  return result;
}

double recursiveDeterminant(matrix_t A) {
  if (A.rows == 1) return A.matrix[0][0];
  if (A.rows == 2)
    return A.matrix[0][0] * A.matrix[1][1] - A.matrix[0][1] * A.matrix[1][0];

  double res = 0;
  for (int i = 0; i < A.columns; i++) {
    matrix_t copy = matrixCutCopy(A, 0, i);
    res += pow(-1, i) * A.matrix[0][i] * recursiveDeterminant(copy);
    s21_remove_matrix(&copy);
  }
  return res;
}

int isNotCorrect(matrix_t A) {
  int res = 1;
  if (A.rows > 0 && A.columns > 0 && A.matrix) res = 0;
  return res;
}