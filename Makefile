CC = gcc
C_LIBS := -lm
TEST_LIBS := -lpthread -lcheck -lm
CFLAGS = -std=c11 -Wall -Werror -Wextra
SOURCES = $(wildcard *.c)
OBJECTS = $(patsubst %.c, %.o, $(SOURCES))

# Проверяем операционную систему
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
    # Для Linux
    CFLAGS += -D LINUX
    TEST_LIBS += -lsubunit
else
    # Для macOS
    CFLAGS += -D MACOS
endif

all: build

build: test

test: clean s21_matrix.a
	$(CC) $(CFLAGS) tests/*.c s21_matrix.a $(TEST_LIBS) -o test
	./test

verter:
	bash ../materials/build/run.sh

s21_matrix.a: $(OBJECTS)
	ar rcs s21_matrix.a $(OBJECTS)
	ranlib s21_matrix.a

gcov_flag:
	$(eval CFLAGS += --coverage)

debug_flag:
    $(eval CFLAGS += -g)

gcov_report: clean gcov_flag test
	mkdir -p gcov
	lcov --capture --directory . --output-file coverage.info
	genhtml coverage.info --output-directory gcov
	rm -rf *.gcno *.gcda *.gcov *.o

debug: clean debug_flag build
	rm -rf *.o

style:
	cp ../materials/linters/.clang-format ../
	clang-format -n *.c *.h 
	clang-format -i *.c *.h 
	clang-format -n *.c *.h 
	clang-format -n tests/*.c tests/*.h
	clang-format -i tests/*.c tests/*.h
	clang-format -n tests/*.c tests/*.h
	rm ../.clang-format

valgrind:
	valgrind --leak-check=full ./test

rebuild: clean all

clean:
	rm -rf *.a *.o tests/*.o test *.a *.gcno *.gcda *.gcov *.info gcov
